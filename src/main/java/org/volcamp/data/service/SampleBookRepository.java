package org.volcamp.data.service;

import java.util.UUID;
import org.springframework.data.jpa.repository.JpaRepository;
import org.volcamp.data.entity.SampleBook;

public interface SampleBookRepository extends JpaRepository<SampleBook, UUID> {

}